package com.example.promenade.ui.home


import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.promenade.BusinessActivity
import com.example.promenade.Model.Annuncio
import com.example.promenade.R
import com.example.promenade.Server.ServerApi
import com.squareup.picasso.Picasso
import com.squareup.picasso.Picasso.LoadedFrom
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.annunci_card_view.view.*
import java.lang.Exception


class AnnunciAdapter(val mActivity: Activity?) : RecyclerView.Adapter<AnnunciAdapter.MyViewHolder>() {

    val server = ServerApi()
    val annunci: ArrayList<Annuncio> = server.getAnnunciAPI()

    class MyViewHolder(val cardView: CardView) : RecyclerView.ViewHolder(cardView)

    //Crea nuove view
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val cardViewAnnunci = LayoutInflater.from(parent.context).inflate(R.layout.annunci_card_view, parent, false) as CardView
        val imageViewPeole = cardViewAnnunci.findViewById<ImageView>(R.id.imageViewPeople)
        imageViewPeole.setImageResource(R.drawable.ic_social_people)
        val textNumPer = cardViewAnnunci.findViewById<TextView>(R.id.textNumeroPersone)
        val textDescr = cardViewAnnunci.findViewById<TextView>(R.id.textDescription)
        val textName = cardViewAnnunci.findViewById<TextView>(R.id.textName)
        textNumPer.setTextSize(20F)
        textDescr.setTextSize(15F)
        textName.setTextSize(20F)
        return MyViewHolder(cardViewAnnunci)
    }


    override fun getItemCount(): Int {
        return annunci.size
    }

    //tiene traccia dell'indice e prende l'elemento a quell'indice
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: AnnunciAdapter.MyViewHolder, position: Int) {
        val annuncio= annunci.get(position)
        holder.cardView.textName.text = annuncio.titolo
        holder.cardView.textDescription.text = annuncio.descrizione
        val imageViewAnnuncio = holder.cardView.findViewById<ImageView>(R.id.imageViewAnnuncio)

        Picasso.get().load(annuncio.immagine).into(imageViewAnnuncio)

        holder.cardView.setOnClickListener {
            val intent = Intent(holder.cardView.context, BusinessActivity::class.java)
            intent.putExtra("position", position)
            holder.cardView.context.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(mActivity).toBundle());
        }
    }

}