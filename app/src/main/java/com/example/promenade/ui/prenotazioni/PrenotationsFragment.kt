package com.example.promenade.ui.prenotazioni

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.promenade.R

class PrenotationsFragment : Fragment() {

    private lateinit var prenotationsViewModel: PrenotationsViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_prenotazioni, container, false)
        val textView: TextView = root.findViewById(R.id.text_gallery)
      //  prenotationsViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = "PRENOTAZIONI"
     //   })
        return root
    }
}
