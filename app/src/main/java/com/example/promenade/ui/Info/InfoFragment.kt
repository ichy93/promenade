package com.example.promenade.ui.Info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.promenade.R


class InfoFragment : Fragment() {

    private lateinit var infoViewModel: InfoViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_info, container, false)
        val textView: TextView = root.findViewById(R.id.textProva)
      //  infoViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = "INFO"
        //})
        return root
    }


}
