package com.example.promenade.Server

import com.example.promenade.Model.Annuncio

open class ServerApi {

    fun getAnnunciAPI(): ArrayList<Annuncio> {
        val annuncio1: Annuncio = Annuncio(1, "MissKebab", "Provate il nostro kebab ai 4 formaggi", "kebabbaro","https://www.negroni.com/sites/negroni.com/files/styles/scale__1440_x_1440_/public/mariuc_milano_il_kebab_allitaliana_thub_0.jpg?itok=KdKY_jP6", "09:00-18:00", 90)
        val annuncio2: Annuncio = Annuncio(2, "Bella Napoli", "Specialità: Pizza margherita", "pizzeria", "https://www.hovogliadidolce.it/wp-content/uploads/2020/04/pizza-in-teglia-fatta-in-casa-senza-impasto-ricetta-Ho-Voglia-di-Dolce-720x480.png", "19:00-23:00", 9000)
        val annuncio3: Annuncio = Annuncio(3, "Balla Che Ti Passa", "Vieni a ballare in pugliaaa", "discoteca", "https://www.comitatolinguistico.com/wp-content/uploads/2016/01/fetta-di-torta-alle-fragole-300x226.jpg" , "23:00-04:00", 7000)
        val annuncio4: Annuncio = Annuncio(4, "Ristorante La Capannina", "Una cena da ricordare a lume di candela fnajsc sacafjbae fefbe fefefebf ef e fef wef ew fwe f ew hkew fe few kfe kf kfw f fe fe e ne nke nk nks nds ndscn  nkdcs nkdcs nkdcs ndcs nk f c sdv kcsd kdc nkcd kdc kdcs nk", "ristorante", "https://media-cdn.tripadvisor.com/media/photo-s/1a/16/32/e3/wine-room-dari-ristorante.jpg",  "19:00-23:00", 6000)
        var arrayAnnunci: ArrayList<Annuncio> = arrayListOf(annuncio1, annuncio2, annuncio3, annuncio4)
        return arrayAnnunci
    }

}