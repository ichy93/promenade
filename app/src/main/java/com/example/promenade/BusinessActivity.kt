package com.example.promenade


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.transition.Explode
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.promenade.Model.Annuncio
import com.example.promenade.Server.ServerApi
import com.squareup.picasso.Picasso
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class BusinessActivity : AppCompatActivity() {

    val server = ServerApi()
    val annunci: ArrayList<Annuncio> = server.getAnnunciAPI()

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.business_collapsing_layout)

        this.getWindow().setExitTransition(Explode())
        setSupportActionBar(findViewById(R.id.toolbar))
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowTitleEnabled(false)
        val imageTool = findViewById<ImageView>(R.id.app_bar_image)
        val businessName = findViewById<TextView>(R.id.prova2)
        businessName.text = "" +
                "Can somebody help me out?\n" +
                "I can't find my feet\n" +
                "I'm sinking in the deep\n" +
                "Can somebody pick me up?\n" +
                "The voice is too loud\n" +
                "I'm losing in the crowd\n" +
                "Because I, can't breathe\n" +
                "Oh, I can't breathe\n" +
                "Because I, can't breathe\n" +
                "Oh come and help me out\n" +
                "Somebody help me out?" +
                "Can somebody help me out?\n" +
                "I can't find my feet\n" +
                "I'm sinking in the deep\n" +
                "Can somebody pick me up?\n" +
                "The voice is too loud\n" +
                "I'm losing in the crowd\n" +
                "Because I, can't breathe\n" +
                "Oh, I can't breathe\n" +
                "Because I, can't breathe\n" +
                "Oh come and help me out\n" +
                "Somebody help me out?"
        val myIntent = intent // gets the previously created intent
        val position: Int = myIntent.getIntExtra("position", 0)
        val annuncio= annunci.get(position)
        Picasso.get().load(annuncio.immagine).into(imageTool)
        //businessName.text = annuncio.titolo*/

    }


    /*
    //animations

    override fun onBackPressed() {
        super.onBackPressed()
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

    }



  */

    fun getBitmapFromURL(src: String?): Bitmap? {
        return try {
            val url = URL(src)
            val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
            connection.setDoInput(true)
            connection.connect()
            val input: InputStream = connection.getInputStream()
            BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            // Log exception
            null
        }
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            //finish()
            //overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            finishAfterTransition();


            return true
        }
        return false
    }









}