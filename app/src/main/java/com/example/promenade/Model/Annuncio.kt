package com.example.promenade.Model

import java.net.URL

data class Annuncio(val id: Int, val titolo: String, val descrizione: String,
                    val categoria: String, val immagine: String, /* maps*/  val orari: String, val telefono: Int)