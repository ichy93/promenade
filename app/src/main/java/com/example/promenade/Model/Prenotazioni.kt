package com.example.promenade.Model

data class Prenotazioni(val id: Int, val locale: String, val utente: String, val orario: String, val persone: Int, val confermata: Boolean)

// orario... string? Date class?